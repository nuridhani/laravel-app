FROM php:apache

WORKDIR /var/www

COPY . /var/www
COPY .env.swarmpit .env

RUN apt update 
RUN apt install -y unzip procps gnupg2 libzip-dev libz-dev libmcrypt-dev libonig-dev zip unzip 

RUN chmod -R 775 storage bootstrap/cache
RUN chown -R root:www-data storage bootstrap/cache

RUN sed -ri -e 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/*.conf

RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash - && apt-get update && apt-get install -y nodejs

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install --no-dev --optimize-autoloader --prefer-dist

RUN docker-php-ext-install bcmath && docker-php-ext-enable bcmath
RUN pecl install -o -f msgpack
RUN pecl install -o -f igbinary
RUN docker-php-ext-enable msgpack igbinary

RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-configure mysqli --with-mysqli=mysqlnd
RUN docker-php-ext-install pdo_mysql mysqli

RUN docker-php-ext-install zip

RUN pecl install -o -f redis &&  docker-php-ext-enable redis

RUN docker-php-ext-configure pcntl --enable-pcntl && docker-php-ext-install pcntl

## Clean repo cache
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN apt-get -y autoremove
RUN apt-get clean
